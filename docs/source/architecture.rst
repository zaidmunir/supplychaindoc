Architecture
============

The system mainly consist of a PaaS component and a SaaS component. 

1. PaaS (Handles deployment and management of blockchain nodes)
2. SaaS (API for easily configuring and setting up network)

PaaS
^^^^
This component essentially takes care of all the hardwork of setting up nodes and forming networks. The nodes are requisitioned in the cloud, each participant gets his own Blockchian node in the cloud. All the permissions for the network are also dealt with. 

SaaS
^^^^
This component provides an interface to the PaaS and also allows for value added and time saving services such as quickly setting up and configuring Blockchain network, setting up access control and permissions for parties. Smart contracts for many many usecases, simply select the asset class that best defines your usecase and the smart contract handles the rest. This component interacts with PaaS and handles all the installation for you.

Diagram
^^^^^^^
.. image:: ../img/architecture.jpg
   :alt: Architecture Diagram 
   :name: Architecture Diagram
   :scale: 50 %
   :align: center

Introduction
============
We simplify the process of supply chain visibility in a multiparty and heteregenous network. We provide an easy to use API that allows all parties to share data that is required to maintain visibility in the supply chain. The parties retain control over the data shared and also can choose what to share and with whom.

Our system manages all aspects of the Blockchain including deploymeny and management of nodes, installing smart contracts and setting up permissions and maintiaining access control for the Blockchain network.

We have a growing list of smart contracts tailored for different asset classes corresponding to real world usescases. There is an asset class type suited for business waiting for you. 

In order to ease onbaording we provide code samples for usecases, these can be used as such or be used to jump start integration into your already exisiting systems.

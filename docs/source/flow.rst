Flow
====

The following sequence diagram will take you through the basic flow of using our system.

Diagram
^^^^^^^^^^^^^^^^
.. image:: ../img/saas.jpg
   :scale: 50 %
   :alt: Flow Diagram
   :align: center


.. Supply chains's documentation master file, created by
   sphinx-quickstart on Mon Oct  7 13:43:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Supply chains's documentation!
=========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   intro
   architecture
   flow

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
